#!/bin/bash
config='/root/.s3cfg'

echo [default] > $config
echo access_key = $s3_access_key >> $config 
echo secret_key = $s3_secret_key >> $config 
echo check_ssl_certificate = True >> $config 
echo guess_mime_type = True >> $config 
echo host_base = $s3_endpoint >> $config 
echo host_bucket = $s3_bucket >> $config
echo use_https = True >> $config
